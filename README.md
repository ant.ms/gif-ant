> **THIS REPOSITORY IS NOW ON [git.ant.lgbt/ConfusedAnt/gif-ant](https://git.ant.lgbt/ConfusedAnt/gif-ant)**

> Checkout the running application here: [gif.ant.lgbt](https://gif.ant.lgbt/)
> Or visit the post on my website for more information: [ant.lgbt/posts/developement/gifant/](https://ant.lgbt/posts/developement/gifant/)

I do not own the copyright to any of the gifs on here