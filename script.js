var gifs;

setTimeout(getGifs, 150);

var myToast = Toastify({
    text: "coppied to clipboard",
    duration: 2000
});

async function getGifs() {
    // read text from URL location
    var request = new XMLHttpRequest();
    request.open('GET', 'data.txt', true);
    request.send(null);
    request.onreadystatechange = function () {
        gifs = request.responseText.split("\n");
        setGifs();
        lazyLoading();
    }
}

function updateTags() {
    tagsToShow = [];
    tagsToNotShow = [];

    for (let i = 0; i < document.getElementsByClassName("form-check-input").length; i++) {
        const element = document.getElementsByClassName("form-check-input")[i];
        if (element.checked == true)
            tagsToShow.push(element.value);
        else
            tagsToNotShow.push(element.value);
    }

    // set all to not display
        for (let j = 0; j < document.getElementsByClassName("card").length; j++)
            document.getElementsByClassName("card")[j].style.display = 'none';


    // check if none are selected
    if (tagsToShow.length != 0) {
        // show all with correct tag
        for (let i = 0; i < tagsToShow.length; i++)
            for (let j = 0; j < document.getElementsByClassName(tagsToShow[i]).length; j++)
                document.getElementsByClassName(tagsToShow[i])[j].style.display = 'unset';
    } else {
        // show all with no tags
        for (let i = 0; i < tagsToNotShow.length; i++)
            for (let j = 0; j < document.getElementsByClassName(tagsToNotShow[i]).length; j++)
                document.getElementsByClassName(tagsToNotShow[i])[j].style.display = 'unset';
    }

    // hide nsfw again (yes, it's inefficient but it works)
    if (document.getElementById("nsfwCheckbox").checked == false) {
        for (let j = 0; j < document.getElementsByClassName("nsfw").length; j++)
            document.getElementsByClassName("nsfw")[j].style.display = 'none';
    }
}

async function setGifs() {
    // await new Promise(resolve => setTimeout(resolve, 200));
    var output = "";
    var tagOutput = "";
    var alreadyAddedTags = ["test"];

    for (let i = 0; i < gifs.length; i++) {
        // add gif
        var content = gifs[i].split(" ");
        output += `<div class="card`;

        // add tags
        for (let j = 1; j < content.length; j++) {
            output += " " + content[j];
            if (alreadyAddedTags.includes(content[j]) == false) {
                alreadyAddedTags.push(content[j]);
                if (content[j] != "nsfw") {
                    tagOutput += `<input class="form-check-input" type="checkbox" id="inlineCheckbox${content[j]}" value="${content[j]}" onchange="updateTags()">`;
                    tagOutput += `<label class="form-check-label" for="inlineCheckbox${content[j]}">${content[j]}</label>`;
                }
            }
        }

        // add the rest of the gif
        output += `" style="display:none" class="a16rema">`;
        output += `<img data-src="${content[0]}" class="card-img-top" onclick="copyToMe('${content[0]}')">`;
        output += `</div></div>`;
    }

    document.getElementById("gifs").innerHTML = output;
    document.getElementById("normal-tags").innerHTML = tagOutput;

    updateTags();
}

function copyToMe(text) {
    element = document.createElement('textarea');
    element.value = text;
    document.body.appendChild(element);
    element.select();
    document.execCommand('copy');
    document.body.removeChild(element);
    myToast.showToast();
}


/* lazy loading */

function lazyLoading() {
    const images = document.querySelectorAll("[data-src]");

    images.forEach(image => {
        imgObserver.observe(image);
    })
}

function preloadImage(img) {
    const src = img.getAttribute("data-src");
    if (!src) return;

    img.src = src;
}

const imgOptions = {
    threshold: 1,
    rootMargin: "0px 0px 500px 0px"
};

const imgObserver = new IntersectionObserver((entries, imgObserver) => {
    entries.forEach(entry => {
        if(!entry.isIntersecting) return;
        else {
            preloadImage(entry.target);
            imgObserver.unobserve(entry.target);
        }
    })
}, imgOptions);